<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
    		'name' => 'Steve James Foncy',
    		'email' => 'steveariasarias@hotmail.com',
    		'password' => bcrypt('foncy12345'),
    		'document' => '71395320',
    		'admin' => 1
        ]);  


        DB::table('users')->insert([
            'name' => 'Cesar ZAnabria',
            'email' => 'cesar@gmail.com',
            'password' => bcrypt('cesar12345'),
            'document' => '71395323',
            'admin' => 2
        ]);  

        DB::table('almacens')->insert([
            'nombre' => 'Almacen Principal',
            'codigo' => '0001A',
            'direccion' => 'Fidel Escudero 192',
            'distrito' => 'San Miguel',
        ]);  


        DB::table('almacens')->insert([
            'nombre' => 'Almacen Alterno',
            'codigo' => '0002A',
            'direccion' => 'Fidel Escudero 192',
            'distrito' => 'Pueblo Libre',
        ]);  


        DB::table('almacens')->insert([
            'nombre' => 'Almacen Tercero',
            'codigo' => '0003A',
            'direccion' => 'Fidel Escudero 192',
            'distrito' => 'Magdalena',
        ]);  


        DB::table('productos')->insert([
            'name' => 'Insumos de alimentos',
            'almacen' => 'Almacen Principal',
            'description' => 'Insumos destinados a la fabricacion de pasta de tomates',
            'fec_vencimiento' => date("y-m-d H:i:s"),
        ]);  

        DB::table('productos')->insert([
            'name' => 'Insumos de Construccion',
            'almacen' => 'Almacen Alterno',
            'description' => 'Insumos destinados a la construccion',
            'fec_vencimiento' => date("y-m-d H:i:s"),
        ]);  

    }
}
