<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_compras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('almacen')->nullable();
            $table->string('status')->default('PRODUCTO_COMPRADO');
            $table->integer('cantidad')->nullable();
            $table->double('costo',7,2)->nullable();
            $table->string('proveedor')->nullable();
            $table->timestamp('fec_compra')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_compras');
    }
}
