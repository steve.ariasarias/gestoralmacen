<?php

use App\Producto;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('almacen')->nullable();
            $table->integer('cantidad')->nullable();
            $table->double('costo',7,2)->nullable();
            $table->double('venta',7,2)->nullable();
            $table->string('status')->default(Producto::PRODUCTO_CREADO);
            $table->timestamp('fec_vencimiento')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
