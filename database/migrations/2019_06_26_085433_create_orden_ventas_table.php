<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_ventas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_producto')->nullable();
            $table->string('codigo_producto')->nullable();
            $table->double('precio_venta',7,2)->nullable();
            $table->integer('cantidad')->nullable();
            $table->string('name_cliente')->nullable();
            $table->string('last_name_cliente')->nullable();
            $table->timestamp('fec_pedido')->nullable(); 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_ventas');
    }
}
