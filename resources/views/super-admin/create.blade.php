@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registrar Almacen Secundario') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('almacen.store') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control" name="nombre" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Codigo') }}</label>

                            <div class="col-md-6">
                                <input id="codigo" type="text" class="form-control" name="codigo" value="{{ old('codigo') }}" required autocomplete="name" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                          <label for="inputState"  class="col-md-4 col-form-label text-md-right">Distrito</label>
                          <div class="col-md-6">                              
                             <select id="distrito" name="distrito" class="form-control">
                                <option selected>Selecciona una opcion ...</option>
                                <option value="San Miguel">San Miguel</option>
                                <option value="Breña"> Breña</option>
                                <option value="Magdalena">Magdalena</option>
                            </select>
                        </div>           
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>

                        <div class="col-md-6">
                            <input id="direccion" type="text" class="form-control" name="direccion" value="{{ old('direccion') }}" required autocomplete="name" autofocus>
                        </div>
                    </div>


                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Registrar almacen') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
