@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="float-left">
                      <h4>Bienvenido <b class="text-success"> {{$title}} </b></h4>
                      <h5>Administracion de <b class="text-primary">Almacenes</b></h5>

                  </div>
                  <div class="float-right">
                      <a class="btn btn-sm btn-success" href="{{ route('almacen.create') }}"> Registrar nuevo almacen secundario</a>
                      <a class="btn btn-sm btn-warning" href="{{ route('user.index') }}">Operadores de almacen</a>
                  </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">ID</th>
                                <th>Codigo</th>
                                 <th>Nombre</th>
                                <th>Direccion</th>
                                <th>Distrito</th>
                                <th colspan="2">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($listAlmacen as $almacen)
                            <tr>
                                <td>{{$almacen->id}}</td>
                                <td>{{$almacen->codigo}}</td>
                                <td>{{$almacen->nombre}}</td>
                                <td>{{$almacen->direccion}}</td>
                                <td>{{$almacen->distrito}}</td>

                                <td width="10px">
                                    <a role="button" href="#" class="btn btn-sm btn-outline-secondary">Editar</a>
                                </td>
                                <td>
                                    <a onclick="return confirm('Estas seguro de eliminarlo??')"  href="{{route('almacen.destroy', $almacen)}}" class="btn btn-sm btn-danger">Eliminar</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection

