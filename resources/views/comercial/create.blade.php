@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registrar Producto') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('product.store') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del Producto') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <label for="inputState"  class="col-md-4 col-form-label text-md-right">Almacen</label>
                            <div class="col-md-6">                              
                             <select id="almacen" name="almacen" class="form-control">
                                <option selected>Selecciona una opcion ...</option>
                                @foreach($listAlmacen as $almacen)
                                    <option value="{{$almacen->nombre}}">{{$almacen->nombre}} {{$almacen->distrito}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>           

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de Registro') }}</label>
                        <div class="col-md-6">
                            <input id="fec_vencimiento" type="date" class="form-control" name="fec_vencimiento"  required >
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>

                        <div class="col-md-6">
                            <textarea id="description" type="textarea" rows="4" cols="50" class="form-control" name="description" value="{{ old('description') }}" required autocomplete="name" autofocus> </textarea> 
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Registrar producto') }}
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
