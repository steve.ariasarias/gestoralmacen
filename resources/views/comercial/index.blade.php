@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="float-left">
            <h4>Bienvenido <b class="text-success"> {{$title}} </b></h4>
            <h5>Registro de nuevos <b class="text-primary">Productos</b> para la venta</h5>
          </div>
          <div class="float-right">
            <a class="btn btn-sm btn-success" href="{{ route('product.create') }}"> Registrar nuevo producto</a>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <th width="10px">ID</th>
                  <th>Nombre</th>
                  <th>Almacen</th>
                  <th>Estado</th>
                  <th>Cantidad</th>
                  <th>Fecha Registro</th>
                  <th class="text-center" colspan="2" style="background-color: black; color:white"> ORDEN DE COMPRA</th>
                  <th class="text-center" colspan="2" style="margin-left: 5px; background-color: yellow; color:red"> GENERAR VENTA</th>
                  <th class="text-center" style="margin-left: 5px; background-color: blue; color:white"> KARDEX</th>

                </tr>
              </thead>

              <tbody>
                @foreach($listProduct as $product)
                <tr>
                  <td>{{$product->id}}</td>
                  <td>{{$product->name}}</td>
                  <td>{{$product->almacen}}</td>
                  
                  @if($product->status === 'nuevo')
                  <td class="text-success"> Nuevo</td>
                  @endif
                  @if($product->status === 'disponible')
                  <td class="text-info"> Disponible</td>
                  @endif
                  @if($product->status === 'venta')
                  <td class="text-danger"> Venta</td>
                  @endif
                  @if($product->cantidad == '')
                  <td> No hay compra</td>
                  @else

                  <td>{{$product->cantidad}}</td>
                  @endif
                  <td>{{$product->fec_vencimiento}}</td>

                  <td width="5px">
                    <a role="button" href="{{ route('orden-compra.generate', $product) }}" class="btn btn-sm btn-outline-secondary">Registrar</a>
                  </td>
                  <td width="5px">
                    <a role="button" href="{{ route('orden-compra.list', $product) }}" class="btn btn-sm btn-outline-primary">Ver Compras</a>
                  </td>
                  

                  <td width="5px">
                    <a role="button" href="{{ route('orden-venta.generate', $product) }}" class="btn btn-sm btn-outline-secondary">Registrar</a>
                  </td>
                  <td width="5px">
                    <a role="button" href="{{ route('orden-venta.list', $product) }}" class="btn btn-sm btn-outline-primary">Ver Ventas</a>
                  </td>
                   <td width="5px">
                    <a role="button" href="{{ route('generate.kardex', $product) }}" class="btn btn-sm btn-danger">Generar Kardex</a>
                  </td>
                  
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

