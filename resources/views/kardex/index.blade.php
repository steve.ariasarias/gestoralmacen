@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="float-left">
            <h3 class="text-center"> Informe de <b class="text-primary">KARDEX</b></h5>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered table-hover text-center">
                <thead>
                  <tr style="background-color:  #c8c1c1 ">
                    <th colspan="2"> Detalle </th>
                    <th colspan="3"> Entradas</th>
                    <th colspan="3"> Salidas </th>
                    <th colspan="3"> Existencias </th>

                  </tr>
                  <tr style="background: #c8c1c1">
                    <th>Fecha</th>
                    <th width="100px">Detalle</th>

                    <th>Cantidad</th>
                    <th>Costo de Produccion Unitario</th>
                    <th>Costo de Produccion Total</th>

                    <th>Cantidad</th>
                    <th>Precio de Venta Unitario</th>
                    <th>Precio de Venta Total</th> 

                    <th>Cantidad</th>
                    <th>Costo de Produccion Unitario</th>
                    <th>Costo de Produccion Total</th> 

                  </tr>
                </thead>

                <tbody>
                  <tr>
                    <td colspan="8" style="background: yellow"> VACIO</td>
                    <td>{{$cantidadInicial}}</td>
                    <td>{{$costoInicial}}</td>
                    <td >@php
                    echo $cantidadInicial* $costoInicial;
                  @endphp</td>
                </tr>
                <tr>
                  <td>{{$compra->fec_compra}}</td>
                  <td> Orden de compra</td>
                  <td>{{$compra->cantidad}}</td>
                  <td>{{$compra->costo}}</td>
                  <td > @php
                  echo $compra->cantidad*$compra->costo;
                @endphp</td>
                <td colspan="3" style="background-color: yellow"> Vacio</td>
                <td>@php
                $cantidadNew = $cantidadInicial+ $compra->cantidad;
                echo $cantidadNew;
              @endphp</td>
              <td>
                @php
                $costoNew= ($cantidadInicial* $costoInicial + $compra->cantidad* $compra->costo)/($cantidadInicial+$compra->cantidad);
                echo $costoNew;
                @endphp
              </td>
              <td>
                @php
                echo $cantidadNew*$costoNew;
                @endphp
              </td>
            </tr>


            <tr>
              <td>{{$venta->fec_pedido}}</td>
              <td> Orden de Venta</td>
              <td colspan="3" style="background-color: yellow"> Vacio</td>
              <td>{{$venta->cantidad}}</td>
              <td>{{$venta->precio_venta}}</td>
              <td > @php
              echo $venta->cantidad*$venta->precio_venta;
            @endphp</td>
            <td>@php
            $cantidadNew2 = $cantidadNew+ $venta->cantidad;
            echo $cantidadNew;
          @endphp</td>
          <td>
            @php
            echo $costoNew;
            @endphp
          </td>
          <td>
            @php
            echo $cantidadNew2*$costoNew;
            @endphp
          </td>
        </tr>

      </tbody>
    </table>
  </div>
</div>
</div>
</div>
</div>
</div>

@endsection

