@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="float-left">
            <h5> Historial de  <b class="text-primary">Orden de Ventas</b></h5>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover text-center">
              <thead>
                <tr>
                  <th width="10px">ID</th>
                  <th>Nombre de Producto</th>
                  <th>Codigo del Producto</th>
                  <th>Cantidad</th>
                  <th>Precio Venta</th>
                  <th>Fecha de Venta</th>
                  <th> Nombre Cliente</th>
                  <th> Apellido Cliente</th>
                  <th> Ruta Optima</th>
                  <th colspan="2"> &nbsp</th>
                </tr>
              </thead>

              <tbody>
                @foreach($listVentas as $venta)
                <tr>
                  <td>{{$venta->id}}</td>
                  <td>{{$venta->name_producto}}</td>
                  <td>{{$venta->codigo_producto}}</td>
                  <td>{{$venta->cantidad}} unidades</td>
                  <td>S/. {{$venta->precio_venta}}</td>
                  <th>{{$venta->fec_pedido}}</th>
                  <td>{{$venta->name_cliente}}</td>
                  <td>{{$venta->last_name_cliente}}</td>

                  <td>
                    <a onclick="return confirm('Estas seguro de eliminarlo??')"  href="{{route('venta.destroy', $venta->id)}}" class="btn btn-sm btn-danger">Eliminar orden de Venta</a>
                  </td>

                  <td>
                    <a target="_blank" href="https://www.google.com/maps/dir/Plaza+San+Miguel,+Avenida+Universitaria,+Lima/Almacen+Promelsa,+Avenida+Guillermo+Dansey,+Cercado+de+Lima/@-12.0612817,-77.0863361,14z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x9105c970336b0725:0xae2a6f969b48786e!2m2!1d-77.0818676!2d-12.0774497!1m5!1m1!1s0x9105c8d4358d8887:0xaa112f817403e628!2m2!1d-77.0531494!2d-12.0458352" class="btn btn-sm btn-info">Ver ruta optima</a>
                  </td>

                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

