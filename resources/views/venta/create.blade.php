@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registrar Orden de Venta') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('orden-venta.store') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del Producto') }}</label>

                            <div class="col-md-6">
                                <input id="name_producto" type="text" class="form-control" name="name_producto" value="{{ $product->name }}" required autocomplete="name" autofocus readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Codigo') }}</label>

                            <div class="col-md-6">
                                <input id="codigo_producto" type="text" class="form-control" name="codigo_producto" value="{{ $product->id }}" required autocomplete="name" autofocus readonly>
                            </div>
                        </div>
                        <div class="row ">
                            <h6  style="background-color: black; color:yellow; margin-left: 6rem; font-size: .8rem; padding: 2px">Costo:   S/.{{$product->costo}}</h5>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Precio de Venta S/.') }}</label>
                            <div class="col-md-6">
                                <input id="precio_venta" type="number" class="form-control" name="precio_venta" required autocomplete="name" autofocus step="any" >
                            </div>
                        </div>
                        <div class="row">
                            <h6  style="background-color: black; color:red; margin-left: 6rem; font-size: .8rem; padding: 2px">Stock:   {{$product->cantidad}} unidades</h5>
                        </div>
                        <div class="form-group row">
                            <label for="cantidad" class="col-md-4 col-form-label text-md-right">{{ __('Cantidad a comprar') }}</label>

                            <div class="col-md-6">
                                <input id="cantidad" type="number" class="form-control" name="cantidad" required autocomplete="name" autofocus >
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de Pedido ') }}</label>
                            <div class="col-md-6">
                                <input id="fec_pedido" type="date" class="form-control" name="fec_pedido"  required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del Cliente') }}</label>

                            <div class="col-md-6">
                                <input id="name_cliente" type="text" class="form-control" name="name_cliente"  required autocomplete="name" autofocus >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Apellido del Cliente') }}</label>

                            <div class="col-md-6">
                                <input id="last_name_cliente" type="text" class="form-control" name="last_name_cliente" required autocomplete="name" autofocus >
                            </div>
                        </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar Venta') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
