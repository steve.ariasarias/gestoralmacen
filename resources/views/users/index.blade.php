@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                     Operadores de almacen   
                        <a class="btn btn-primary btn-sm float-right" href="{{route('user.create')}}"> Registrar Operadores+</a>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th width="10px">ID</th>
                                    <th>Nombres</th>
                                    <th>Email</th>
                                    <th>DNI</th>
                                    <th colspan="3">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->document}}</td>
                                        <td width="10px">
                                                <a role="button" href="{{route('user.show',$user->id)}}" class="btn btn-sm btn-outline-secondary">Ver más</a>
                                           
                                        </td>
                                        <td width="10px">
                                                <a role="button" href="{{route('user.edit',$user->id)}}" class="btn btn-sm btn-outline-secondary">Modificar</a>
                                        </td>

                                        <td width="10px">
                                               {!!Form::open(['route'=>['user.destroy',$user->id],'method'=>'DELETE'])!!}
                                                    <button class="btn btn-sm btn-danger">
                                                        Eliminar
                                                    </button>
                                                {!!Form::close()!!}

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{$users->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
