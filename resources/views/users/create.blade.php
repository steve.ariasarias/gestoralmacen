@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Registrando opearador de Almacen
                </div>
                <div class="card-body">                
                        {!!Form::open(['route'=>'user.store'])!!}
                            @include('users.partial.form')
                        {!!Form::close()!!}        

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
