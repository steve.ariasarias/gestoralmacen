<div class="form-group">
 {{Form::label('name','Nombres')}}
 {{Form::text('name',null,['class'=>'form-control'])}}
</div>
<div class="form-group">
 {{Form::label('last_name','Apellidos')}}
 {{Form::text('last_name',null,['class'=>'form-control'])}}
</div>


<div class="form-group">
	{{Form::label('document','Número de documento')}}
	{{Form::number('document',null,['class'=>'form-control','id'=>'document_number'])}}
</div>


<div class="form-group">
 {{Form::label('email','Correo electrónico')}}
 {{Form::text('email',null,['class'=>'form-control'])}}
</div>

<div class="form-group">
	{{Form::label('password','Contraseña')}}
	{{Form::input('password', 'password')}}
</div>



<div class="form-group">
 {{Form::submit('Guardar',['class'=>'btn btn-sm btn-primary float-right'])}}
</div>