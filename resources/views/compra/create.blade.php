@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registrar Orden de Compra') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('orden-compra.store') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre del Producto') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $product->name }}" required autocomplete="name" autofocus readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Codigo') }}</label>

                            <div class="col-md-6">
                                <input id="codigo" type="text" class="form-control" name="codigo" value="{{ $product->id }}" required autocomplete="name" autofocus readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Costo S/.') }}</label>

                            <div class="col-md-6">
                                <input id="costo" type="number" class="form-control" name="costo" required autocomplete="name" autofocus step="any"  >
                            </div>
                        </div>

                           <div class="form-group row">
                            <label for="cantidad" class="col-md-4 col-form-label text-md-right">{{ __('Cantidad') }}</label>

                            <div class="col-md-6">
                                <input id="cantidad" type="number" class="form-control" name="cantidad" required autocomplete="name" autofocus  >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de Registro') }}</label>
                            <div class="col-md-6">
                                <input id="fec_vencimiento" type="datetime" class="form-control" name="fec_vencimiento"  value="{{$product->fec_vencimiento}}" required  readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Fecha de Compra ') }}</label>
                            <div class="col-md-6">
                                <input id="fec_compra" type="date" class="form-control" name="fec_compra"  required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputState"  class="col-md-4 col-form-label text-md-right">Proveedor</label>
                            <div class="col-md-6">                              
                               <select id="proveedor" name="proveedor" class="form-control">
                                <option selected>Selecciona una opcion ...</option>
                                <option value="Proveedor1">Proveedor 1</option>
                                <option value="Proveedor1">Proveedor 2</option>
                                <option value="Proveedor1">Proveedor 3</option>
                            </select>
                        </div>
                    </div>           

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Registrar Compra') }}
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
