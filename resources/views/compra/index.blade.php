@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="float-left">
            <h5> Historial de  <b class="text-primary">Orden de Compras</b></h5>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <th width="10px">ID</th>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  <th>Costo</th>
                  <th>Fecha de Compra</th>
                  <th>Proveedor</th>
                  <th colspan="1"> &nbsp</th>
                </tr>
              </thead>

              <tbody>
                @foreach($listOrders as $order)
                <tr>
                  <td>{{$order->id}}</td>
                  <td>{{$order->name}}</td>
                  <td>{{$order->cantidad}}</td>
                  <td>{{$order->costo}}</td>
                  <th>{{$order->fec_compra}}</th>
                  <td>{{$order->proveedor}}</td>

                  <td>
                    <a onclick="return confirm('Estas seguro de eliminarlo??')"  href="{{route('orden.destroy', $order->id)}}" class="btn btn-sm btn-danger">Eliminar orden Compra</a>
                  </td>

                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

