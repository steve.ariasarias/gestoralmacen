<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Almacen extends Model
{

    
    protected $fillable = [
    	'id',
        'nombre',
        'codigo',
        'direccion',
        'distrito',
        'latitud',
        
		'longitud',
    ];


}
