<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    //
	const PRODUCTO_DISPONIBLE = 'disponible';
	const PRODUCTO_VENTA = 'venta';
	const PRODUCTO_NO_DISPONIBLE = 'no disponible';
    const PRODUCTO_CREADO = 'nuevo';
	protected $fillable = [
		'name',
		'almacen',
		'status',
		'description',
		'fec_vencimiento',
		'cantidad',
		'costo'
	];

}
