<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdenCompra extends Model
{
    //
    const PRODUCTO_DISPONIBLE = 'disponible';
	protected $fillable = [
		'name',
		'status',
		'fec_compra',
		'proveedor',
		'cantidad',
		'costo'
	];

}
