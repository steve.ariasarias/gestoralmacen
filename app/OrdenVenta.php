<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdenVenta extends Model
{
    //
	protected $fillable = [
		'name_producto',
		'codigo_producto',
		'precio_venta',
		'cantidad',
		'name_cliente',
		'last_name_cliente',
		'fec_pedido',
	];
}
