<?php

namespace App\Http\Controllers\Almacen;

use App\Almacen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlmacenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('super-admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $reglas = [
            'nombre' => 'required',
            'codigo' => 'required',
            'direccion' => 'required',
            'distrito' => 'required'
        ];
    
        $this->validate($request, $reglas);
        $campos = $request->all();
        $usuario = Almacen::create($campos);
          
        return redirect()->route('home')->with('info','Se ha registrao un nuevo almacen secundario con exito');
    }


    public function destroy(Almacen $almacen)
    {
        $almacen->delete();
        return back()->with('info','Eliminado correctamente');

    }
}
