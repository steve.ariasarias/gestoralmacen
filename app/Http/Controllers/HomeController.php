<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Almacen;
use App\Producto;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if(Auth::check()){
            $userLogged=$request->user();
            if($userLogged->admin == 1){ //  Jefe de product
                $almacens = Almacen::all();    
                return view('super-admin.index')->with('listAlmacen', $almacens)->withTitle('Jefe de product');;
            } 
            if($userLogged->admin == 2){ // Personal del area comercial
                $products = Producto::all();
               return view('comercial.index')->with('listProduct',$products)->withTitle('Personal del Area Comercial');
            }
        }
        return view('home');
    }
}
