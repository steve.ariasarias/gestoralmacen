<?php

namespace App\Http\Controllers;

use App\OrdenCompra;
use App\Producto;
use Illuminate\Http\Request;

class OrdenCompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $product)
    {
        //
        //dd($product->name);
        //return view('compra.create')->with('product', $product);

    }
    public function showOrder( Producto $product)
    {
        return view('compra.create')->with('product', $product);

    }

     public function listOrders( Producto $product)
    {

        $orders=  OrdenCompra::where('name', '=', $product->name)->paginate();
        return view('compra.index')->with('listOrders',  $orders);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $campos = $request->all();
        $campos['status']='CREADO';
        OrdenCompra::create($campos);
         
        $product = Producto::where('name','=', $request->name);

        $newProduct = array();
        $newProduct['cantidad'] = $request->cantidad;
        $newProduct['costo'] = $request->costo;
        $newProduct['status'] = Producto::PRODUCTO_DISPONIBLE;
        
        $product->update($newProduct); 
        return redirect()->route('product.index')->with('info','Se ha registrado la compra del producto '. $request->name. ' con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrdenCompra  $ordenCompra
     * @return \Illuminate\Http\Response
     */
    public function show(OrdenCompra $ordenCompra)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrdenCompra  $ordenCompra
     * @return \Illuminate\Http\Response
     */
    public function edit(OrdenCompra $ordenCompra)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrdenCompra  $ordenCompra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrdenCompra $ordenCompra)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrdenCompra  $ordenCompra
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $orden = OrdenCompra::find($id);
        $orden->delete();
        return back()->with('info','Se ha eliminado la orden de compra con exito');

    }
}
