<?php

namespace App\Http\Controllers;

use App\OrdenVenta;
use App\OrdenCompra;
use App\Producto;
use Illuminate\Http\Request;

class OrdenVentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function showOrder( Producto $product)
    {
        return view('venta.create')->with('product', $product);

    }

    public function listOrders( Producto $product)
    {

        $ventas=  OrdenVenta::where('name_producto', '=', $product->name)->paginate();
        return view('venta.index')->with('listVentas',  $ventas);

    }

    public function generateKardex(Producto $product){
        
       $lastVenta=  OrdenVenta::where('name_producto', '=', $product->name)->orderBy('created_at','DESC')->take(1)->get();
       $lastCompra= OrdenCompra::where('name', '=', $product->name)->orderBy('created_at','DESC')->take(1)->get();
       
       if(count($lastVenta) != 0 && count($lastCompra) != 0){
           
           $cantidadInicial= $lastCompra[0]->cantidad*3;
           $costoInicial= $lastCompra[0]->costo*2;
           return view('kardex.index')->with('venta', $lastVenta[0])->with('compra', $lastCompra[0])->with('cantidadInicial',$cantidadInicial)->with('costoInicial', $costoInicial);     
       }
       else{
        return back()->with('info','Debes tener como minimo una compra y venta para generar el Kardex');
    }
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        $campos = $request->all();
        OrdenVenta::create($campos); 
        
        $product = Producto::where('name','=', $request->name_producto);
        $newProductObject=$product->get();
        
        
        if($request->cantidad >  $newProductObject[0]->cantidad ) {
            return back()->with('info','La cantidad ingresada no puede ser mayor al stock');
        }
        else{
            $newProduct = array();
            $newProduct['cantidad'] = $newProductObject[0]->cantidad - $request->cantidad ;
            $newProduct['status'] = Producto::PRODUCTO_VENTA;
            $product->update($newProduct); 
            return redirect()->route('product.index')->with('info','Se ha registrado la venta del producto '. $request->name. ' con exito');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrdenVenta  $ordenVenta
     * @return \Illuminate\Http\Response
     */
    public function show(OrdenVenta $ordenVenta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrdenVenta  $ordenVenta
     * @return \Illuminate\Http\Response
     */
    public function edit(OrdenVenta $ordenVenta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrdenVenta  $ordenVenta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrdenVenta $ordenVenta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrdenVenta  $ordenVenta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       $venta = OrdenVenta::find($id);
       $venta->delete();
       return back()->with('info','Se ha eliminado la orden de venta con exito');
   }
}
