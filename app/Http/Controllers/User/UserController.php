<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$this->middleware('permission:users.index');
        $users= User::where('admin', '!=', '1')->paginate();
        
        return view('users.index',compact('users'));
    }

    public function create()
    {
        //
        return view('users.create');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $reglas = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed'
        ];

        //$this->validate($request, $reglas);
        
        $campos = array();
        $campos['name'] = $request->name . " ". $request->last_name;
        $campos['email'] = $request->email;
        $campos['document'] = $request->document;
        $campos['password'] = Hash::make($request->password);

        $usuario = User::create($campos);

        return redirect()->route('user.index')->with('info','Se ha registrao un nuevo usuario con exito');
    }




    public function update(Request $request, User $user)
    {
        //Actualice el usuario
        $user->update($request->all());

        return redirect()->route('user.index')->with('info','Se han registrado las modificaciones del usuario con exito');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        $user->delete();
        return back()->with('info','Se ha eliminado el usuario seleccionado con exito');
    }
}
