<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('user', 'User\UserController');


//Route::resource('almacen','Almacen\AlmacenController');

Route::get('almacen/{almacen}','Almacen\AlmacenController@destroy')->name('almacen.destroy');
Route::post('almacen','Almacen\AlmacenController@store')->name('almacen.store');
Route::get('almacen','Almacen\AlmacenController@create')->name('almacen.create');
	

Route::resource('product', 'Producto\ProductoController');

Route::resource('orden-compra', 'OrdenCompraController');
Route::get('orden/generate/{product}','OrdenCompraController@showOrder')->name('orden-compra.generate');
Route::get('orden/list/{product}','OrdenCompraController@listOrders')->name('orden-compra.list');
Route::get('orden/{order}','OrdenCompraController@destroy')->name('orden.destroy');



Route::resource('orden-venta', 'OrdenVentaController');
Route::get('venta/generate/{product}','OrdenVentaController@showOrder')->name('orden-venta.generate');
Route::get('venta/list/{product}','OrdenVentaController@listOrders')->name('orden-venta.list');
Route::get('venta/{order}','OrdenVentaController@destroy')->name('venta.destroy');


Route::get('kardex/{product}','OrdenVentaController@generateKardex')->name('generate.kardex');


//Route::resource('orden-venta', 'OrdenVentaController');